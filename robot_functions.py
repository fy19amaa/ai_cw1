from argparse import ArgumentError
from robot import *
import queue
import random
from pprint import pprint

GOAL_ROOM_CONTENTS = dict()

ROOM_CONTENTS = {
        'workshop'     : {'rusty key'},
        'store room'   : {'bucket', 'suitcase'},
        'tool cupboard' : {'sledge hammer', 'anvil', 'saw', 'screwdriver'},
}

DOORS = [
        Door('workshop', 'store room' ),
        Door( 'store room', 'tool cupboard', doorkey='rusty key', locked=False )
]

possible_items = [
    '💌', '🔪', '🧭', '⏰', '🪀',
    '📻', '📠', '💻', '🧮', '📺',
    '📷', '💡', '📗', '📘', '📙',
    '📓', '💰', '💳', '📁', '📅',
    '📌', '📏', '📐', '🔑', '🔨',
    '🪓', '🔫', '🪃', '🪚', '🔧',
    '🪛', '🪜', '🧪', '🔬', '🔭',
    '🪒', '🧹', '🧻', '🧼', '🪥',
    '👞', '👠', '🩴', '👟', '👖',
    '👕', '🧥', '🎲', '🎱', '🥝',
    '🥭', '🍋', '🍍', '🍇', '🍒',
]

possible_places = [
    '⛺', '🚽', '🏫', '🏡', '🛖',
    '🏥', '⛲', '🏦', '🗻', '🌋',
    '🏪', '🏬', '🏭', '🏯', '🪐',
]

def getEdges(room_contents:dict, doors: 'list[Door]'):
    edges = dict()
    pprint(room_contents)
    pprint(doors)
    pprint(ITEM_WEIGHT)

    for room in room_contents.keys():
        edges[room] = []

    for door in doors:
        roomA, roomB = door.goes_between
        edges[roomA].append(roomB)
        edges[roomB].append(roomA)

    return edges

def minPathsBFS(edges:dict, start:str) -> 'dict[list]':
    visited = {e: False for e in edges.keys()}
    paths = dict()
    Q = queue.Queue()

    paths[start] = list()
    Q.put(start)
    visited[start] = True

    while not Q.empty():
        current = Q.get()

        for neighbour in edges[current]:
            if visited[neighbour]:
                continue

            if not paths[current]:
                paths[neighbour] = [current]
            else:
                paths[neighbour] = paths[current].copy()
                paths[neighbour].append(current)

            Q.put(neighbour)

        visited[current] = True

    return paths

def getPathsDistancesMatrix(edges: dict) -> 'tuple[ dict[frozenset, list], dict[frozenset, int] ]':
    paths = dict()
    distances = dict()

    for node in edges.keys():
        minimum_paths = minPathsBFS(edges, node)
        for vertex in edges.keys():
            key = frozenset((node, vertex))
            if key in paths.keys():
                continue
            # get minimum PATH between node & vertex
            paths[key] = minimum_paths[vertex]
            # get LENGTH of minimum path between node & vertex
            distances[key] = len(minimum_paths[vertex])

    return paths, distances

def updateGLOBALS(goal_item_locations:dict, room_contents:dict, doors: 'list[Door]'):
    global GOAL_ROOM_CONTENTS
    global ROOM_CONTENTS
    global DOORS
    global EDGES
    global PATHS
    global DISTANCES

    GOAL_ROOM_CONTENTS = goal_item_locations
    ROOM_CONTENTS = room_contents
    DOORS = doors
    EDGES = getEdges(ROOM_CONTENTS, DOORS)
    PATHS, DISTANCES = getPathsDistancesMatrix(EDGES)

############# heuristics and cost #############

def misplaced_items(state: State):
    remaining = 0

    for goal_room in GOAL_ROOM_CONTENTS.keys():
        for item in GOAL_ROOM_CONTENTS[goal_room]:
            if item not in state.room_contents[goal_room]:
                remaining += 1

    return remaining

def misplaced_weights(state: State):
    remaining = 0
    for goal_room in GOAL_ROOM_CONTENTS.keys():
        for goal_item in GOAL_ROOM_CONTENTS[goal_room]:
            if goal_item not in state.room_contents[goal_room]:
                remaining += ITEM_WEIGHT[goal_item]+1 # +1 to avoid 0
    return remaining

def weighted_distance(state: State):
    global DISTANCES
    remaining = 0

    goal_item_room = {
        item:room for room, items in state.room_contents.items() for item in items
    }

    for goal_room in GOAL_ROOM_CONTENTS.keys():
        for goal_item in GOAL_ROOM_CONTENTS[goal_room]:
            if goal_item in state.room_contents[goal_room]:
                continue

            remaining += ITEM_WEIGHT[goal_item]+1

            if goal_item in state.robot.carried_items:
                continue

            target_current_location = goal_item_room[goal_item]
            remaining += DISTANCES[frozenset((target_current_location, goal_room))]

    return remaining

def cost(path, state):
        return len(path)

def generate_rooms_goals(rooms_count:int,
                         items_count:int,
                         goal_rooms_count:int,
                         goal_items_count:int,
                         doors_count:int,
                         robot_strength:int):
    if doors_count < rooms_count-1:
        raise Exception('doors_count must be > rooms_count-1.')

    if doors_count > rooms_count*(rooms_count-1)//2:
        raise Exception('doors_count must be <= rooms_count*(rooms_count-1)//2.')

    if goal_rooms_count > rooms_count:
        raise Exception('doors_count must be <= rooms_count.')

    if goal_items_count > items_count:
        raise Exception('goal_items_count must be <= items_count.')

    global possible_items
    global possible_places
    global ITEM_WEIGHT

    # setting up rooms..

    choosen_rooms = random.sample(possible_places, k=rooms_count)
    choosen_items = random.sample(possible_items, k=items_count)

    room_items = {room:set() for room in choosen_rooms}

    for item in choosen_items:
        random_i = random.randint(0, len(choosen_rooms)-1)
        room_items[choosen_rooms[random_i]].add(item)

    # setting up weights..
    ITEM_WEIGHT = {item:random.randint(0, robot_strength) for item in choosen_items}
    updateITEM_WEIGHT_robot(ITEM_WEIGHT)

    # setting up goals..

    goal_rooms = random.sample(choosen_rooms, k=goal_rooms_count)
    goal_items = random.sample(choosen_items, k=goal_items_count)

    goal_room_items = {goal_room:set() for goal_room in goal_rooms}

    for item in goal_items:
        random_i = random.randint(0, len(goal_rooms)-1)
        goal_room_items[goal_rooms[random_i]].add(item)

    # setting up doors..
    doors = [Door(roomA=choosen_rooms[0], roomB=choosen_rooms[1])]
    chosen_pairs_already = [{choosen_rooms[0], choosen_rooms[1]}]

    for i in range(2, rooms_count):
        roomA, roomB = choosen_rooms[i], choosen_rooms[random.randint(0, i-1)]
        while {roomA, roomB} in chosen_pairs_already:
            roomA, roomB = choosen_rooms[i], choosen_rooms[random.randint(0, i-1)]

        doors.append(Door(roomA=roomA, roomB=roomB))
        chosen_pairs_already.append({roomA, roomB})

    for _ in range(doors_count-rooms_count-1):
        roomA, roomB = goal_rooms = random.sample(choosen_rooms, k=2)
        while {roomA, roomB} in chosen_pairs_already:
            roomA, roomB = goal_rooms = random.sample(choosen_rooms, k=2)

        doors.append(Door(roomA=roomA, roomB=roomB))
        chosen_pairs_already.append({roomA, roomB})

    # robot initial settings..
    start_room = random.choice(choosen_rooms)

    return room_items, goal_room_items, doors, start_room