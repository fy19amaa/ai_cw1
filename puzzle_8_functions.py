import random


NORMAL_GOAL_POSITIONS = {
    1:(0,0), 2:(0,1), 3:(0,2),
    4:(1,0), 5:(1,1), 6:(1,2),
    7:(2,0), 8:(2,1), 0:(2,2)
}

## A couple of possible goal states:
NORMAL_GOAL = [ [1,2,3],
                [4,5,6],
                [7,8,0] ]

SPIRAL_GOAL = [ [1,2,3],
                [8,0,4],
                [7,6,5] ]

### Declarations of some example states
LAYOUT_0 = [ [1,5,2],
             [0,4,3],
             [7,8,6] ]

LAYOUT_1 = [ [5,1,7],
             [2,4,8],
             [6,3,0] ]

LAYOUT_2 = [ [2,6,3],
             [4,0,5],
             [1,8,7] ]

LAYOUT_3 = [ [7,2,5],
             [4,8,1],
             [3,0,6] ]

LAYOUT_4 = [ [8,6,7],
             [2,5,4],
             [3,0,1] ]

# ====================== heuristics ======================= #

def number_position_in_layout( n, layout):
    for i, row in enumerate(layout):
        for j, val in enumerate(row):
            if val==n:
                return (i,j)


def misplaced_tiles(state):
    result = 0
    for n in range(9):
        for i in range(3):
            for j in range(3):
                if n == state[1][i][j]:
                    row, col = i, j
                    break
        if (row, col) != NORMAL_GOAL_POSITIONS[n]: result += 1
    return result


def manhattan(state):
    distance = 0
    for n in range(9):
        for i in range(3):
            for j in range(3):
                if n == state[1][i][j]:
                    row, col = i, j
                    break
        goal_row, goal_col = NORMAL_GOAL_POSITIONS[n]
        distance += abs(row - goal_row) + abs(col - goal_col)
    return distance


def cost(path, state):
        return len(path)

# ====================== solvable ======================= #

def minSwaps(arr):
    n = len(arr)

    # Create two arrays and use as pairs where first array
    # is element and second array is position of first element
    arrpos = [*enumerate(arr)]

    # Sort the array by array element values to get right position of
    # every element as the elements of second array.
    arrpos.sort(key = lambda it : it[1])

    # To keep track of visited elements. Initialize all elements as not visited or false.
    vis = {k : False for k in range(n)}

    # Initialize result
    ans = 0
    for i in range(n):

        # alreadt swapped or
        # alreadt present at
        # correct position
        if vis[i] or arrpos[i][0] == i:
            continue

        # find number of nodes
        # in this cycle and
        # add it to ans
        cycle_size = 0
        j = i

        while not vis[j]:

            # mark node as visited
            vis[j] = True

            # move to next node
            j = arrpos[j][0]
            cycle_size += 1

        # update answer by adding
        # current cycle
        if cycle_size > 0:
            ans += (cycle_size - 1)

    # return answer
    return ans

def getInvCount(arr):
    inv_count = 0
    empty_value = 0
    for i in range(0, 9):
        for j in range(i + 1, 9):
            if arr[j] != empty_value and arr[i] != empty_value and arr[i] > arr[j]:
                inv_count += 1
    return inv_count

def is_solvable_8puzzle(layout):
    inv_count = getInvCount([j for sub in layout for j in sub])
    # return true if inversion count is even.
    return (inv_count % 2 == 0)

def generate_random_layout(maximum_inversions=18):
    # assuming the maximum number of inversions of 9 element array is 18
    layout =[
            [1,2,3],
            [4,5,6],
            [7,8,0]
            ]
    done_inversions = set()

    n = len(layout)

    numbers = list(range(n*n))

    solvable = False
    while not solvable:
        for _ in range(maximum_inversions//2):
            rps = random.sample(numbers, k=4)

            if {rps[0], rps[1]} in done_inversions or {rps[2], rps[3]} in done_inversions:
                continue

            layout[rps[0]//n][rps[0]%n], layout[rps[1]//n][rps[1]%n] = layout[rps[1]//n][rps[1]%n], layout[rps[0]//n][rps[0]%n]
            layout[rps[2]//n][rps[2]%n], layout[rps[3]//n][rps[3]%n] = layout[rps[3]//n][rps[3]%n], layout[rps[2]//n][rps[2]%n]

            done_inversions.add(frozenset([rps[0], rps[1]]))
            done_inversions.add(frozenset([rps[2], rps[3]]))

        solvable = is_solvable_8puzzle(layout)

    return layout

# ====================== main ======================= #

def main():
    # LAYOUT_RAND = generate_random_layout(18)
    # layout_list = [j for sub in LAYOUT_RAND for j in sub]
    # print(f'{LAYOUT_RAND = }')
    # print(f'{layout_list = }')

    # print(f'{is_solvable_8puzzle(LAYOUT_RAND) = }')

    # print(f'{getInvCount(layout_list) = }')
    # print(f'{minSwaps(layout_list) = }')

    # print(manhattan((0, LAYOUT_ABDUL, 0)))
    pass

if __name__ == '__main__':
    main()