from bbSearch import *
from robot import *
from robot_functions import *
import csv

algos = {
    'BFS': ('BF/FIFO', None, None, False),
    'DFS': ('DF/LIFO', None, None, False),
    'DFS (rand)': ('DF/LIFO', None, None, True),
    'B-F [DF] (mis-weight)': ('DF', None, misplaced_weights, False),
    'B-F [DF] (weight-dis)': ('DF', None, weighted_distance, False),
    'B-F [BF] (mis-weight)': ('BF', None, misplaced_weights, False),
    'B-F [BF] (weight-dis)': ('BF', None, weighted_distance, False),
    'A* [DF] (mis-weight)': ('DF', cost, misplaced_weights, False),
    'A* [DF] (weight-dis)': ('DF', cost, weighted_distance, False),
    'A* [BF] (mis-weight)': ('BF', cost, misplaced_weights, False),
    'A* [BF] (weight-dis)': ('BF', cost, weighted_distance, False),
}

short_tc = {"GOAL_STATE_FOUND"     : "Y",
            "NODE_LIMIT_EXCEEDED"  : "!",
            "SEARH-SPACE_EXHAUSTED": "x"}

def print_test_results(filename, results):
    algo_names = list(algos.keys())
    with open(filename, 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(['Test', 'Max', 'Result', 'Path length', '# Nodes tested', 'Time (s)'])
        for i, test in enumerate(results):
            Max  = test['args']['max_nodes']
            tc  = test['result']['termination_condition']
            stc = short_tc[tc]

            path_len = test['result']['path_length']
            if path_len == None: path_len = 'N/A'

            nt  = test['search_stats']['nodes_tested']
            time = round( test['search_stats']['time_taken'], 5 )

            writer.writerow([algo_names[i], Max, stc, path_len, nt, time])

def test_algos(puzzle):
    results = []
    for algo in algos.values():
        results.append(
            search(
                puzzle, algo[0], 25000, cost=algo[1], heuristic=algo[2], randomise=algo[3],
                loop_check=True, show_path=False, show_state_path=False, return_info=True, dots=False
            )
        )

    return results

def main():
    for i in range(1,4):
        print('==================== SETTING UP... ====================')
        robot_strength = 10
        ROOM_CONTENTS, goal_item_locations, DOORS, start_room = generate_rooms_goals(
            rooms_count=5*i,
            items_count= int(55/3*i),
            goal_rooms_count= int(2/3 * 5*i),
            goal_items_count= int(3/4 * 55/3*i),
            doors_count=int((5*i)*(5*i-1)/4),
            robot_strength=robot_strength
        )

        rob = Robot(start_room, [], robot_strength )

        state = State(rob, DOORS, ROOM_CONTENTS)

        robot_servant = RobotServant( state, goal_item_locations )

        updateGLOBALS(goal_item_locations, ROOM_CONTENTS, DOORS)

        print(f'==================== TESTING{i} ====================')
        results = test_algos(robot_servant)
        print_test_results(f'robotcsv/test{i}.csv', results)
        # print(f'''rooms_count= {5*i},
        #     items_count= {int(55/3*i)},
        #     goal_rooms_count= {int(2/3 * 5*i)},
        #     goal_items_count= {int(3/4 * 55/3*i)},
        #     doors_count= {int((5*i)*(5*i-1)/4)},
        #     robot_strength= {robot_strength}''')

if __name__ == "__main__":
    main()



