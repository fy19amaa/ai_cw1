from bbSearch import *
from puzzle_8_functions import *
import csv

ALL_POSSIBLE_SOLVABLE_STATES = 181440 + 1 # 1 to be safe

Testing_Layouts = [
    [
        [1,2,3],
        [4,0,5],
        [7,8,6],
    ],
    [
        [2,0,8],
        [3,7,4],
        [1,6,5],
    ],
    generate_random_layout(20),
    generate_random_layout(20),
]

algos = {
    'BFS': ('BF/FIFO', None, None, False), # BFS
    'DFS': ('DF/LIFO', None, None, False), # DFS
    'DFS (rand)': ('DF/LIFO', None, None, True), # DFS (random)
    'B-F (mis)': ('BF/FIFO', None, misplaced_tiles, False), # Best-First (misplaced_tiles)
    'B-F (man)': ('BF/FIFO', None, manhattan, False), # Best-First (manhattan)
    'A* (mis)': ('DF/LIFO', cost, misplaced_tiles, False), # A* (misplaced_tiles)
    'A* (man)': ('DF/LIFO', cost, manhattan, False), # A* (manhattan)
}

short_tc = {"GOAL_STATE_FOUND"     : "Y",
            "NODE_LIMIT_EXCEEDED"  : "!",
            "SEARH-SPACE_EXHAUSTED": "x"}

def print_test_results(filename, level, results):
    algo_names = list(algos.keys())
    with open(filename, 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(['Test', 'Level', 'Result', 'path length', '# Nodes tested', 'Time (s)'])
        for i, test in enumerate(results):
            tc  = test['result']['termination_condition']
            stc = short_tc[tc]

            path_len = test['result']['path_length']
            if path_len == None: path_len = 'N/A'

            nt  = test['search_stats']['nodes_tested']
            time = round( test['search_stats']['time_taken'], 5 )

            writer.writerow([algo_names[i], level, stc, path_len, nt, time])

def test_algos(puzzle, loop_check):
    global algos
    results = []

    for algo in algos.values():
        results.append(
            search(
                puzzle, algo[0], 10000, cost=algo[1], heuristic=algo[2], randomise=algo[3],
                loop_check=loop_check, show_path=False, show_state_path=False, return_info=True, dots=False
            )
        )

    return results

def main():
    levels = ['Easy', 'Hard', 'Random', 'Random']
    for i, layout in enumerate(Testing_Layouts):
            print(f'==================== TEST{i+1}... ====================')
            puzzle = EightPuzzle( layout, NORMAL_GOAL )

            results = test_algos(puzzle, False)
            print_test_results(f'puzzle8csv/test{i+1}_NO_loop_check.csv',levels[i], results)

            results = test_algos(puzzle, True)
            print_test_results(f'puzzle8csv/test{i+1}_loop_check.csv',levels[i], results)

if __name__ == "__main__":
    main()